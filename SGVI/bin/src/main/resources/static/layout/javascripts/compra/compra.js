$('#numero-ingressos').on('keypress', function (event) {
    var regex = new RegExp("^[a-zA-Z0-9]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
       event.preventDefault();
       return false;
    }
});


$('#setor').focusout(function(){
	
	if($('#numero-ingressos').val() != "" && $('#setor').val() != ""){
			
		let total = 0;
		let ingressos = parseInt(($('#numero-ingressos').val()), 10);
		let valor = parseInt(($('#setor').val()), 10);
		total = valor * ingressos;
		
		$('#valor-total').val("R$ " + total);
	}
	
})

$('#numero-ingressos').focusout(function(){
	
	if($('#numero-ingressos').val() != "" && $('#setor').val() != ""){
			
		let total = 0;
		let ingressos = parseInt(($('#numero-ingressos').val()), 10);
		let valor = parseInt(($('#setor').val()), 10);
		total = valor * ingressos;
		
		$('#valor-total').val("R$ " + total);
	}
	
})

$('.btn-compra').click(function(){
	
	Swal.fire({
		  type: 'success',
		  title: 'Compra concluída',
		  text: 'Sua compra foi realizada com sucesso. Basta acessar seu perfil para imprimir o seu ingresso!',
		  showConfirmButton: false,
		  showCloseButton: true,
		  focusConfirm: false,
		  footer: 'SourceTickets'
		})
		.then(function() {
		    window.location.href = "/inicial";
		})
	
})

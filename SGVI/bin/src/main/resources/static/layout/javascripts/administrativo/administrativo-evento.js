$(document).ready(function(){
	$.ajax({
		type: 'GET',
		url: "/evento/getAllEventos",
		async: true,
		success: function(data){
			let aux = 0;
			for(aux = 0; aux < data.length; aux++){
				$('#lista-eventos').append(`<p class="lista-opcao" id="` + data[aux].id + `">` + `<span class="ml-3">` + data[aux].nome + `</span> </p>`)
			}
		}
	});
	
})
$('#btn-add-show').click(function(){
	$('#card-administracao-inicio').hide();
	$('#card-administracao-evento').show();
})

$('#btn-voltar-1-evento').click(function(){
	$('#card-administracao-inicio').show();
	$('#card-administracao-evento').hide();
})

$(document).on('keypress', '#evento-nome', function(e) {
    if(e.which == 13) {
        $('#btn-pesquisar-evento').trigger('click');
    }
});

$('#btn-pesquisar-evento').click(function(){
	let nomeEvento = " ";
	nomeEvento = $('#evento-nome').val();
	console.log(nomeEvento)
	
	if(nomeEvento == "" || nomeEvento == null || nomeEvento == undefined )
		{
			nomeEvento="todos";
		}
	
	$.ajax({
		type: 'GET',
		url: `/evento/getEventos/${nomeEvento}`,
		async: true,
		success: function(data){
			console.table(data)
			$('#lista-eventos').empty();
			let aux = 0;
			for(aux = 0; aux < data.length; aux++){
				$('#lista-eventos').append(`<p class="lista-opcao"> <span class="ml-3">` + data[aux].nome + `</span> </p>`)
			}
		}
		
	})
	
})

$('#btn-excluir-evento').click(function(){
	
	if($('.clicked').attr('id') == null || $('.clicked').attr('id') == undefined || $('.clicked').attr('id') == ""){
		
	}else{
		let id = $('.clicked').attr('id');
		
		Swal.fire({
			  title: 'Deletar Evento',
			  text: "Você tem certeza disso?",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#4e002f',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Sim',
			  cancelButtonText: 'Não',
			  footer: 'SourceTickets'
			}).then((result) => {
			  if (result.value) {
				  $.ajax({
						type: 'POST',
						url: `/administrativo/evento/deletarEvento/${id}`,
						async: true,
						success: function(data){
							Swal.fire({
								title: 'Deletado!',
							      text: 'O evento foi deletado com sucesso',
							      icon: 'success',
							      confirmButtonColor: '#4e002f',
							      footer: 'SourceTickets'
								
							}).then(function(){
								
								window.location = "/administrativo"
								
							})	
								      
						}
					})
			  }
			})
	}
	
})

$('#btn-excluir-setor').click(function(){
	
	$('#lista-setores').empty()
	$('#lista-setores').append(`<div class="row mt-3">
								<div class="col-md-12">
								<input type="text" class="form-control" name = "setor.descricao" placeholder="Descrição do setor">
								</div>
							</div>
							<div class = "row mt-3">
								<div class="col-md-6">
								<input type="text" class="form-control" name = "setor.valor" placeholder="Valor do setor">
								</div>
								
								<div class="col-md-6">
								<input type="text" class="form-control" name = "setor.ingresosDisponiveis" placeholder="quantidade de ingressos">
								</div>
							</div>`)
	
})

$('#btn-add-setor').click(function(){
	
	$('#lista-setores').append(`<div class="row mt-3">
								<div class="col-md-12">
								<input type="text" class="form-control" name = "setor.descricao" placeholder="Descrição do setor">
								</div>
							</div>
							<div class = "row mt-3">
								<div class="col-md-6">
								<input type="text" class="form-control" name = "setor.valor" placeholder="Valor do setor">
								</div>
								
								<div class="col-md-6">
								<input type="text" class="form-control" name = "setor.ingresosDisponiveis" placeholder="quantidade de ingressos">
								</div>
							</div>`)
	
})

$('#evento-novo-proximo').click(function(){
	$('#card-administracao-evento-novo').hide();
	$('#card-administracao-evento-novo-2').show();
})

$('#evento-novo-proximo-2').click(function(){
	$('#card-administracao-evento-novo-2').hide();
	$('#card-administracao-evento-novo-3').show();
})


$('#btn-voltar-3-evento').click(function(){
	$('#card-administracao-evento-novo-2').hide();
	$('#card-administracao-evento-novo').show();
})

$('#btn-voltar-2-evento').click(function(){
	$('#card-administracao-evento-novo').hide();
	$('#card-administracao-evento').show();
})

$('#btn-adicionar-evento').click(function(){
	$('#card-administracao-evento').hide();
	$('#card-administracao-evento-novo').show();
})

$('#btn-evento-cadastrar').click(function(){
	
	let dataInicio = $('#data-inicio').val();
	let dataFim = $('#data-termino').val();
	let horaInicio = $('#hora-inicio').val();
	let horaFim = $('#hora-termino').val();
	
	dataInicio = dataInicio + " " + horaInicio;
	dataFim = dataFim + " " + horaFim;
	$('#data-inicio').val(dataInicio);
	$('#data-termino').val(dataFim);
	
	
	
})

//--------------------------CIDADES E ESTADOS ---------------------------------------
//Estados
$(document).ready(function(){
	$('#estado').show();
	$.ajax({
		type: 'POST',
		url: "/livre/getAllEstados",
		async: true,
		success: function(data){			
			$('#estado').append(`<option value="" selected>Estado</option>`)
			data.forEach(function(evento){
				$('#estado').append(`<option value="` + evento.id + `">` + evento.uf + `</option>`)
			})
		}
	});
})

//Cidades
$('#estado').click(function(){
	$('#cidade').empty();
	var estado = $( "#estado option:selected" ).val();
	if(estado == null || estado == undefined || estado == "")
	{
		
	}
	else
	{
		$.ajax({
			type: 'POST',
			url: `/livre/getCidade/${estado}`,
			async: true,
			success: function(data){
				data.forEach(function(cidade){
					$('#cidade').append(`<option value="` + cidade.id + `">` + cidade.nome + `</option>`)
				})
			}
		})
	}
})

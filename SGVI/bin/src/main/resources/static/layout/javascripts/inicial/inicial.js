$(document).ready(function(){
	$('#accordion').show();
	$.ajax({
		type: 'GET',
		url: "/evento/getAllEventos",
		async: true,
		success: function(data){			
			data.forEach(function(evento){
				$('#accordion').append(`
                    <div class="card">
                        <div class="card-header">
                            <a class="card-link" data-toggle="collapse" href="#collapse` + evento.id + `">
                                Nome do Evento: ` + evento.nome + `
                            </a>
                        </div>
                        <div id="collapse` + evento.id + `" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        <span class="badge badge-primary badge-pill">DATA E HORÁRIO DE INÍCIO: </span>
                                        ` + evento.horaInicio + `
                                    </li>
                                    <li class="list-group-item">
                                        <span class="badge badge-primary badge-pill">DATA E HORÁRIO DE TÉRMINO: </span>
                                        ` + evento.horaTermino + `
                                    </li>
                                    <li class="list-group-item">
                                        <span class="badge badge-primary badge-pill">LOCAL: </span>
                                        ` +
                                        	evento.endereco.rua + ` Nº ` + evento.endereco.numero + `, ` + evento.endereco.bairro + `, ` +
                                        			evento.endereco.cidade.nome + ` - ` +
                                        				evento.endereco.cidade.estado.uf
                                        + `
                                    </li>
                                    <li class="list-group-item">
                                        <span class="badge badge-primary badge-pill">DESCRIÇÃO: </span>
                                        ` + evento.descricao + `
                                    </li>
                                    <li class="list-group-item">
                                        <span class="badge badge-primary badge-pill">VALORES DOS INGRESSOS: </span>
                                        <span id="ingressos` + evento.id + `"></span>
                                        <button type="button" class="btn btn-primary btn-sm">COMPRE AGORA</button>
                                    </li>
                                </ul>
                            </div>
                        </div>						
                    </div>`
                )
            	evento.setor.forEach(function(setor) {
					$('#ingressos' + evento.id).append(setor.descricao + `: R$ ` + setor.valor + ` | ` )
				})
			})
		}
	});
})

$('.search-btn').click(function(){
	$('#accordion').empty();
	let eventos = $('#pesquisar-evento').val();
	if(eventos == "" || eventos == null || eventos == undefined )
	{
			eventos = "todos";
	}
	
	$.ajax({
		type: 'GET',
		url: `/evento/getEventos/${eventos}`,
		async: true,
		success: function(data)
		{
			data.forEach(function(evento){
				$('#accordion').append(`
                    <div class="card">
                        <div class="card-header">
                            <a class="card-link" data-toggle="collapse" href="#collapse` + evento.id + `">
                                Nome do Evento: ` + evento.nome + `
                            </a>
                        </div>
                        <div id="collapse` + evento.id + `" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        <span class="badge badge-primary badge-pill">DATA E HORÁRIO DE INÍCIO: </span>
                                        ` + evento.horaInicio + `
                                    </li>
                                    <li class="list-group-item">
                                        <span class="badge badge-primary badge-pill">DATA E HORÁRIO DE TÉRMINO: </span>
                                        ` + evento.horaTermino + `
                                    </li>
                                    <li class="list-group-item">
                                        <span class="badge badge-primary badge-pill">LOCAL: </span>
                                        ` +
                                        	evento.endereco.rua + ` Nº ` + evento.endereco.numero + `, ` + evento.endereco.bairro + `, ` +
                                        			evento.endereco.cidade.nome + ` - ` +
                                        				evento.endereco.cidade.estado.uf
                                        + `
                                    </li>
                                    <li class="list-group-item">
                                        <span class="badge badge-primary badge-pill">DESCRIÇÃO: </span>
                                        ` + evento.descricao + `
                                    </li>
                                    <li class="list-group-item">
                                        <span class="badge badge-primary badge-pill">VALORES DOS INGRESSOS: </span>
                                        <span id="ingressos` + evento.id + `"></span>
                                        <button type="button" class="btn btn-primary btn-sm">COMPRE AGORA</button>
                                    </li>
                                </ul>
                            </div>
                        </div>						
                    </div>`
                )
            	evento.setor.forEach(function(setor) {
					$('#ingressos' + evento.id).append(setor.descricao + `: R$ ` + setor.valor + ` | ` )
				})
			})
		}
	})
	
	$('#accordion').show();
})
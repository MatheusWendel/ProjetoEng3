package com.fatec.mogi.viewHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.fatec.mogi.model.Cliente;
import com.fatec.mogi.util.Util;

public class ClienteVH implements InterfaceViewHelper {

	@Override
	public Cliente montar(HttpServletRequest dadosEntidade) {

		Cliente cliente = new Cliente();
		UsuarioVH usuarioVH = new UsuarioVH();
		EnderecoVH enderecoVH = new EnderecoVH();

		// CRIA OBJETO UTILIZADO PARA FORMATAR A DATA NO FOMATO DA CONSTANTE :
		// FORMATO_DATA
		SimpleDateFormat formataData = new SimpleDateFormat(Util.FORMATO_DATA);

		// CRIA VARIAVEL DE DATA QUE RECEBE A DATA CONVERTIDA PARA O FORMATO CORRETO NO
		// TRY CATCH
		Date dataNascimento = null;
		try {
			dataNascimento = formataData.parse(dadosEntidade.getParameterValues("dataNascimento")[0]);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		cliente.setNome(dadosEntidade.getParameterValues("nome")[0]);
		cliente.setCpf((dadosEntidade.getParameterValues("cpf")[0]));
		cliente.setTelefone(dadosEntidade.getParameterValues("telefone")[0]);
//		cliente.setDataNascimento(dataNascimento);
		cliente.setIngressoBonus(0);
		cliente.setUsuario(usuarioVH.montar(dadosEntidade));
		cliente.setEndereco(enderecoVH.montar(dadosEntidade));

		return cliente;
	}

}

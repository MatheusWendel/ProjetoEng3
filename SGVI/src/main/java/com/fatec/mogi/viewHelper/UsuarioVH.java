package com.fatec.mogi.viewHelper;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.fatec.mogi.model.Permissao;
import com.fatec.mogi.model.Usuario;

public class UsuarioVH implements InterfaceViewHelper{

	@Override
	public Usuario montar(HttpServletRequest dadosEntidade) {
		
		 BCryptPasswordEncoder encriptaSenha = new BCryptPasswordEncoder();
		Usuario usuario = new Usuario();
		String senhaEncriptada = encriptaSenha.encode(dadosEntidade.getParameterValues("senha")[0]);
		
		usuario.setEmail(dadosEntidade.getParameterValues("email")[0]);
		usuario.setSenha(senhaEncriptada);
		usuario.setPermissao(Permissao.USUARIO);
		
		return usuario;
		
	}


	
	
	
}

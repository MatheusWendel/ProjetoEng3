package com.fatec.mogi.viewHelper;

import javax.servlet.http.HttpServletRequest;

import com.fatec.mogi.model.Estado;

public class EstadoVH implements InterfaceViewHelper {

	@Override
	public Estado montar(HttpServletRequest dadosEntidade) {
		
		Estado estado = new Estado();
		
		estado.setId(Long.parseLong(dadosEntidade.getParameterValues("estado")[0]));
		
		return estado;
	}

}

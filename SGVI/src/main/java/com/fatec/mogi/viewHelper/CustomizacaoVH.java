package com.fatec.mogi.viewHelper;

import javax.servlet.http.HttpServletRequest;

import com.fatec.mogi.model.Customizacao;

public class CustomizacaoVH implements InterfaceViewHelper {

	@Override
	public Customizacao montar(HttpServletRequest dadosEntidade) {

		Customizacao customizacao = new Customizacao();
		
		customizacao.setDescricao(dadosEntidade.getParameterValues("descricao")[0]);
		
		return customizacao;
		
		
	}

}

package com.fatec.mogi.viewHelper;

import javax.servlet.http.HttpServletRequest;

import com.fatec.mogi.model.Endereco;

public class EnderecoVH implements InterfaceViewHelper {

	@Override
	public Endereco montar(HttpServletRequest dadosEntidade) {

		Endereco endereco = new Endereco();
		CidadeVH cidadeVH = new CidadeVH();
		
		endereco.setBairro(dadosEntidade.getParameterValues("bairro")[0]);
		endereco.setCep(dadosEntidade.getParameterValues("cep")[0]);
		endereco.setNumero(dadosEntidade.getParameterValues("numero")[0]);
		endereco.setCidade(cidadeVH.montar(dadosEntidade));
		
		
		return endereco;
	}

}

package com.fatec.mogi.viewHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.fatec.mogi.model.Compra;
import com.fatec.mogi.util.Util;

public class CompraVH implements InterfaceViewHelper {

	@Override
	public Compra montar(HttpServletRequest dadosEntidade) {
		Compra compra = new Compra();
		
		
		SimpleDateFormat formataData = new SimpleDateFormat(Util.FORMATO_DATA);

		
		// CRIA VARIAVEL DE DATA QUE RECEBE A DATA CONVERTIDA PARA O FORMATO CORRETO NO
		// TRY CATCH
		Date dataCompra = null;
		Date dataValidadeCartao = null;
		
		try {
			dataCompra = formataData.parse(dadosEntidade.getParameterValues("dataCompra")[0]);
			dataValidadeCartao = formataData.parse(dadosEntidade.getParameterValues("dataValidadeCartao")[0]);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//TODO
//		compra.setCliente(cliente);
		compra.setCvvCartao(dadosEntidade.getParameterValues("cvvCartao")[0]);
		compra.setDataCompra(dataCompra);
		compra.setDataValidadeCartao(dataValidadeCartao);
		compra.setNomeTitularCartao(dadosEntidade.getParameterValues("nomeTitularCartao")[0]);
		compra.setNumeroCartao(dadosEntidade.getParameterValues("numeroCartao")[0]);
		
		return compra;
	}

}

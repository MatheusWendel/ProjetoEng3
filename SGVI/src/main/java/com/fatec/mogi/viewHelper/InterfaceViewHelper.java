package com.fatec.mogi.viewHelper;

import javax.servlet.http.HttpServletRequest;

import com.fatec.mogi.model.BaseModel;

public interface InterfaceViewHelper {

	public BaseModel montar(HttpServletRequest dadosEntidade);

}

package com.fatec.mogi.viewHelper;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.fatec.mogi.model.Artista;

public class ArtistaVH implements InterfaceViewHelper {

	@Override
	public Artista montar(HttpServletRequest dadosEntidade) {
		Artista artista = new Artista();
		
		artista.setDescricao(dadosEntidade.getParameterValues("descricaoArtista")[0]);
		artista.setGenero(dadosEntidade.getParameterValues("genero")[0]);
		artista.setNome(dadosEntidade.getParameterValues("nomeArtista")[0]);
		
		return artista;
	}
	
	

	public List<Artista> montarLista(HttpServletRequest dadosEntidade){
		
		List<Artista> listaArtista = new ArrayList<Artista>();
		String[] idArtistas = dadosEntidade.getParameterValues("idArtista");
		for (int i = 0; i < idArtistas.length; i++) {
			Artista artista = new Artista();
			artista.setId(Long.parseLong(idArtistas[i]));
			listaArtista.add(artista);	
		}
		
		return listaArtista;
	}
}

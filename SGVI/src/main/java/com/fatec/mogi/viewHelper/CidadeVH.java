package com.fatec.mogi.viewHelper;

import javax.servlet.http.HttpServletRequest;

import com.fatec.mogi.model.Cidade;

public class CidadeVH implements InterfaceViewHelper {

	@Override
	public Cidade montar(HttpServletRequest dadosEntidade) {

		Cidade cidade = new Cidade();
		EstadoVH estadoVh = new EstadoVH();

		cidade.setId(Long.parseLong(dadosEntidade.getParameterValues("cidade")[0]));
		cidade.setEstado(estadoVh.montar(dadosEntidade));
		return cidade;
	}

}

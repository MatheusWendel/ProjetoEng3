package com.fatec.mogi.viewHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.fatec.mogi.model.Evento;
import com.fatec.mogi.util.Util;

public class EventoVH implements InterfaceViewHelper {

	@Override
	public Evento montar(HttpServletRequest dadosEntidade) {
		
		Evento evento = new Evento();
		ArtistaVH artistaVH = new ArtistaVH();
		CustomizacaoVH customizacaoVH = new CustomizacaoVH();
		EnderecoVH enderecoVH = new EnderecoVH();
		
		SimpleDateFormat formataData = new SimpleDateFormat(Util.FORMATO_DATA);
		
		Date horaInicio = null;
		Date horaTermino = null;
		
		try {
			horaInicio = formataData.parse(dadosEntidade.getParameterValues("horaInicio")[0]);
			horaInicio = formataData.parse(dadosEntidade.getParameterValues("horaInicio")[0]);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		evento.setArtista(artistaVH.montarLista(dadosEntidade));
		evento.setCustomizacao(customizacaoVH.montar(dadosEntidade));		
		evento.setEndereco(enderecoVH.montar(dadosEntidade));
		evento.setHoraInicio(horaInicio);
		evento.setHoraTermino(horaTermino);
		evento.setNome(dadosEntidade.getParameterValues("nomeEvento")[0]);
		
		
		return evento;
	}
	
	public List<Evento> montarLista(HttpServletRequest dadosEntidade){
		
		List<Evento> listaEvento = new ArrayList<Evento>();
		String[] idEventos = dadosEntidade.getParameterValues("idEventos");
		for (int i = 0; i < idEventos.length; i++) {
			Evento evento = new Evento();
			evento.setId(Long.parseLong(idEventos[i]));
			listaEvento.add(evento);	
		}
		
		return listaEvento;
	}

}

package com.fatec.mogi.controllers;



import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fatec.mogi.command.CommandSave;
import com.fatec.mogi.model.Usuario;
import com.fatec.mogi.viewHelper.UsuarioVH;

@Controller
@RequestMapping("/usuario")
public class UsuarioController {

	@Autowired
	CommandSave commandSave ;
	
		
	@RequestMapping(value = "/cadastro", method = RequestMethod.POST)
	public @ResponseBody void cadastroUsuari(HttpServletRequest dadosUsuario) {
		
		UsuarioVH vh = new UsuarioVH();
		
		Usuario usuario = vh.montar(dadosUsuario);
		commandSave.execute(usuario);
		
		//System.out.println(usuario.getClass().getName());

	}
}

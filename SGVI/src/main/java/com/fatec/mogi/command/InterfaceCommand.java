package com.fatec.mogi.command;

import com.fatec.mogi.model.BaseModel;

public interface InterfaceCommand {
	
	public String execute(BaseModel baseModel);

}
